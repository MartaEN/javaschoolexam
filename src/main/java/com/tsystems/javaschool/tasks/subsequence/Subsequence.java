package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null ) throw new IllegalArgumentException();
        if (x.size() == 0) return true;
        int i = y.indexOf(x.get(0));
        if (y.indexOf(x.get(0)) == -1 ) return false;
        return find(x.subList(1, x.size()), y.subList(i + 1, y.size()));
    }
}
