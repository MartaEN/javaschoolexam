package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws CannotBuildPyramidException if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {

        try {
            int height = height(inputNumbers.size());

            int [][] pyramid = new int [height][height + height - 1];

            Integer[] inputSorted = new Integer[inputNumbers.size()];
            Arrays.sort(inputNumbers.toArray(inputSorted));

            int counter = 0;
            for (int level = 0; level < height; level++) {
                int offset = level;
                for (int elementQty = 1; elementQty <= level + 1 ; elementQty++) {
                    pyramid[level] [height - 1 - offset] = inputSorted[counter++];
                    offset -= 2;
                }
            }
            return pyramid;

        } catch (Exception e) {
            throw new CannotBuildPyramidException();
        }
    }


    /**
     * Checks whether pyramid can be built of given number of elements
     * One can be built if there's an integer n where n + (n-1) + ... 1 = (n+1)*n/2 = number of total available elements
     * @param size - number of building blocks to be used
     * @return - height of the pyramid if is it feasible, or exception in opposite case
     * @throws CannotBuildPyramidException in case pyramid can't be built of given number of elements
     */
    private int height (int size) {
        double n = (Math.sqrt((double)( 1 + 8 * size )) - 1)/2;
        if( n % 1.0 > 0.00001) throw new CannotBuildPyramidException();
        return (int) n;
    }


}
