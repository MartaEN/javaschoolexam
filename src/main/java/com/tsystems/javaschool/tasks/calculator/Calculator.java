package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Locale;

import static java.lang.Double.NEGATIVE_INFINITY;
import static java.lang.Double.NaN;
import static java.lang.Double.POSITIVE_INFINITY;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        try {
            //validating the input and parsing it to LinkedList of elements
            LinkedList<String> list = parseStatement(statement);

            //evaluating and rounding the result
            double d = Double.parseDouble(evaluate(list));
            return formatToString(d);

        } catch (IllegalArgumentException e) {
            //return null in case parsing failed
            return null;
        }
    }

    //checking input data and parsing it to LinkedList containing of opening and closing brackets, arithmetic operators and numbers
    private LinkedList<String> parseStatement (String statement) throws IllegalArgumentException {

        //ensuring input exists
        if(statement == null || statement.equals("")) throw new IllegalArgumentException("Null input");
        //ensuring input only consists of valid symbols
        if(!statement.matches("\\A[\\d\\Q.\\E\\Q+\\E\\Q-\\E\\Q*\\E\\Q/\\E\\Q(\\E\\Q)\\E]+\\Z"))
            throw new IllegalArgumentException("Invalid symbols");
        //ensuring max of one decimal point in numbers
        if(statement.matches(".*\\Q.\\E\\d*\\Q.\\E.*"))
            throw new IllegalArgumentException("Invalid number formats");
        //ensuring operators are not duplicated
        if(statement.matches(".*[\\Q+\\E\\Q-\\E\\Q*\\E\\Q/\\E][\\Q+\\E\\Q-\\E\\Q*\\E\\Q/\\E].*"))
            throw new IllegalArgumentException("Invalid operator placement");
        //ensuring the statement is not ended with an operator
        if(statement.matches(".*[\\Q+\\E\\Q-\\E\\Q*\\E\\Q/\\E]$"))
            throw new IllegalArgumentException("Invalid operator placement");

        // split the input String into array and upload its elements to a LinkedList, while checking syntax
        statement = statement.replaceAll("([\\Q(\\E\\Q)\\E\\Q*\\E\\Q/\\E\\Q+\\E\\Q-\\E])", " $1 ");
        String[] tokens = statement.split(" +");

        LinkedList<String> elements = new LinkedList<>(); // target LinkedList to keep operands
        int bracketCounter = 0; //counter to check brackets for consistency
        boolean openingPosition = true; //flag to check valid operands sequencing
                                        //true - opening brackets and number are expected,
                                        //false - closing brackets and operators are expected

        for (String token: tokens) {
            switch (token) {
                case "(":
                    if ( openingPosition ) {
                        bracketCounter++;
                        elements.add(token);
                    } else throw new IllegalArgumentException("Invalid bracket placement");
                    break;
                case ")":
                    if( !openingPosition && bracketCounter > 0) {
                        bracketCounter--;
                        elements.add(token);
                    } else throw new IllegalArgumentException("Invalid bracket placement");
                    break;
                case "*":
                case "/":
                    if( !openingPosition ) {
                        elements.add(token);
                        openingPosition = true;
                    } else throw new IllegalArgumentException("Invalid operator placement");
                    break;
                case "+":
                case "-":
                    if( openingPosition ) elements.add("0");
                    elements.add(token);
                    openingPosition = true;
                    break;
                default:
                    if( openingPosition) {
                        elements.add(token);
                        openingPosition = false;
                    } else throw new IllegalArgumentException("Invalid operand placement");
            }
        }

        //if not all of the brackets have been closed - parsing fails
        if(bracketCounter > 0)
            throw new IllegalArgumentException("Invalid bracket placement");

        return elements;
    }

    //This method evaluates the result taking pre-processed linked list of operands as input
    private String evaluate (LinkedList<String> operands){

        //the first priority - recursive evaluation of statements in brackets
        processBrackets(operands);

        //the second priority - multiplication and division
        processOperandsInPairs(operands.listIterator(), "[\\Q*\\E\\Q/\\E]");

        //the third priority - addition and substraction
        processOperandsInPairs(operands.listIterator(),"[\\Q+\\E\\Q-\\E]");

        return operands.getFirst();
    }

    //This method searches for bracketed sub-statements and replaces them with their evaluation results in the original list
    private void processBrackets(LinkedList<String> operands) {

        ListIterator<String> iterator = operands.listIterator();

        while (iterator.hasNext()) {

            if (iterator.next().equals("(")) {

                int bracketsCount = 1;
                iterator.remove();
                LinkedList <String> sublist = new LinkedList<>();
                String currentElement;

                while(bracketsCount!=0) {
                    switch (currentElement = iterator.next()) {
                        case ")":
                            if(--bracketsCount == 0) {
                                iterator.set(evaluate(sublist));
                            } else {
                                sublist.add(currentElement);
                                iterator.remove();
                            }
                            break;
                        case "(":
                            bracketsCount++;
                        default:
                            sublist.add(currentElement);
                            iterator.remove();
                    }
                }
            }
        }
    }

    //This methods searches the operators using the given pattern,
    //uses the two numbers just before and just after this operator to evaluate the result
    //and replaces these three elements with the result of the operation in the original list
    private void processOperandsInPairs(ListIterator<String> iterator, String operatorPattern) {
        while (iterator.hasNext()) {
            if (iterator.next().matches(operatorPattern)) {
                iterator.previous();
                double num1 = Double.parseDouble(iterator.previous());
                iterator.remove();
                String operator = iterator.next();
                iterator.remove();
                double num2 = Double.parseDouble(iterator.next());
                iterator.set(String.valueOf(calculate(num1, operator, num2)));
            }
        }
    }

    //This method runs requested arithmetic operation with two given numbers
    private double calculate(double d1, String operator, double d2) {
        switch (operator) {
            case "+": return d1 + d2;
            case "-": return d1 - d2;
            case "*": return d1 * d2;
            case "/": return d1 / d2;
            default: throw new RuntimeException("unknown arithmetic operator");
        }
    }

    //This method formats the result according to the requirement (dot as decimal mark,
    //rounding to 4 significant digits, NaN and Infinity result to null in return
    private String formatToString(double d) {
        if (d == NaN || d == NEGATIVE_INFINITY || d == POSITIVE_INFINITY) return null;
        DecimalFormatSymbols mySymbols = new DecimalFormatSymbols(Locale.getDefault());
        mySymbols.setDecimalSeparator('.');
        mySymbols.setGroupingSeparator(',');
        DecimalFormat df = new DecimalFormat("#.####", mySymbols);
        return df.format(d);
    }

}
